#!/bin/bash
set -euxo pipefail

# following steps as noted on https://hackmd.io/ZbUOkeIXTjaP7XVpTSsrNw?view

# Get OS data.
source /etc/os-release

ID=${ID:-}
ARCH=$(arch)
UUID=${UUID:-local}
OS_VERSION="${OS_VERSION:-9}"
TF_SSH_KEY="${TF_SSH_KEY:-}"

BUCKET=${BUCKET:-}
REGION=${REGION:-}
if [[ "${STREAM}" == "upstream" ]]; then
  OS_PREFIX="cs"
else
  OS_PREFIX="rhel"
fi
REPO_DIRNAME="${OS_PREFIX}${OS_VERSION}"
REPO_URL="https://${BUCKET}.s3.${REGION}.amazonaws.com/${UUID}/${REPO_DIRNAME}"
TARGET=${TARGET:-qemu}
IMAGE_NAME=${IMAGE_NAME:-neptune}
IMAGE_TYPE=${IMAGE_TYPE:-ostree}
FORMAT=${FORMAT:-img}
DISK_IMAGE="cs9-${TARGET}-${IMAGE_NAME}-${IMAGE_TYPE}.${ARCH}.${FORMAT}"
IMAGE_FILE=${IMAGE_FILE:-"/var/lib/libvirt/images/auto-osbuild-${OS_PREFIX}${OS_VERSION}-${ARCH}-${UUID}.raw"}
LOCAL_REPO_DIR=${REPO_DIR:-"/var/lib/repos"}/${REPO_DIRNAME}

STREAM=${STREAM:-upstream}
if [[ "${STREAM}" == "downstream" ]]; then
  OS_OPTIONS=(osname=\"rhivos\" uefi_vendor=\"redhat\")
else
  OS_OPTIONS=(osname=\"centos\" uefi_vendor=\"centos\")
fi

cd osbuild-manifests/osbuild-manifests/
if [[ "${UUID}" != "local" ]]; then
  # install osbuild and osbuild-tools, which contains osbuild-mpp utility
  dnf -y copr enable @osbuild/osbuild
  SEARCH_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/@osbuild/osbuild/epel-${OS_VERSION}-\$basearch/"
  REPLACE_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/@osbuild/osbuild/centos-stream-${OS_VERSION}-\$basearch/"
  sed -i -e "s|$SEARCH_PATTERN|$REPLACE_PATTERN|" \
    /etc/yum.repos.d/_copr\:copr.fedorainfracloud.org\:group_osbuild\:osbuild.repo

  #FIXME: add flatpak-selinux package to workaround issues with the osbuild-selinux package
  dnf -y install flatpak-selinux
  dnf -y install osbuild osbuild-tools osbuild-ostree make

  # enable neptune copr repo
  dnf -y copr enable pingou/qtappmanager-fedora
  SEARCH_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/epel-${OS_VERSION}-\$basearch/"
  REPLACE_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/centos-stream-${OS_VERSION}-\$basearch/"
  sed -i -e "s|$SEARCH_PATTERN|$REPLACE_PATTERN|" \
    /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:pingou:qtappmanager-fedora.repo

  # Set the pipeline's yum-repo
  echo "[+] Using the pipeline's yum-repo:"
  echo "repo: ${REPO_URL}"
  OS_OPTIONS+=(ssh_permit_root_login="true" display_server=\"xorg\" cs${OS_VERSION}_baseurl=\"${REPO_URL}\")
  make ${DISK_IMAGE} \
       DEFINES="${OS_OPTIONS[*]}" \
       MPP_ARGS="-D 'root_ssh_key=\"${TF_SSH_KEY}\"'"
else
  REVISION="main"

  # enable neptune copr repo
  sudo dnf -y copr enable pingou/qtappmanager-fedora
  SEARCH_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/epel-${OS_VERSION}-\$basearch/"
  REPLACE_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/centos-stream-${OS_VERSION}-\$basearch/"
  sudo sed -i -e "s|$SEARCH_PATTERN|$REPLACE_PATTERN|" \
    /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:pingou:qtappmanager-fedora.repo

  make ${DISK_IMAGE} DEFINES="cs${OS_VERSION}_baseurl=\"${LOCAL_REPO_DIR}\" ${OS_OPTIONS[*]}"
fi

echo "[+] Moving the generated image"
sudo mkdir -p $(dirname $IMAGE_FILE)
sudo mv $DISK_IMAGE $IMAGE_FILE

# record some details of the input manifest, and disk image in json
cat <<EOF | sudo tee -a ${IMAGE_FILE%.*}.json
{
  "image_file": "${IMAGE_FILE}",
  "arch": "${ARCH}",
  "os_version": "${OS_PREFIX}${OS_VERSION}",
  "variables": [
    {
      "UUID": "${UUID}",
      "REPO_URL": "${REPO_URL}",
      "REVISION": "${REVISION}",
      "ID": "${ID}"
    }
  ]
}
EOF

# Clean up
echo "[+] Cleaning up"
sudo rm -fr image_output osbuild_store

echo "The final image is here: ${IMAGE_FILE}"
echo "Information about image is in ${IMAGE_FILE%.*}.json"
echo
